menuitem *loadmenu(menu &menusystem)
{
  menuitem *setup;
  menuitem *turn;
  menuitem *yourturn;

  vector<int> num(9);
  vector<string> str(9);
  vector<int> lit(9, 0);
  vector<int> blank;

    
  // YOURTURN menu.
  yourturn  = menusystem.makemenu("YOURTURN");
  menusystem.additem("Yourturn", "Move\nAttack\nWait\n", 1, 0, 3, NULL);
  menusystem.actionlist(beforea);
    //
    str.at(1)="yourturn";
    lit.at(1)=1;
    menusystem.addaction("getxy", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    lit.assign(9, 0);
    //
    menusystem.addaction("getunitmap", num, str, lit, blank, "", blank, "", "");
    //
    str.at(2)="yourx";
    lit.at(2)=1;
    str.at(3)="youry";
    lit.at(3)=1;
    num.at(4)=0;
    num.at(5)=0;
    num.at(6)=0;
    num.at(7)=0;
    num.at(8)=1;
    menusystem.addaction("gettargets", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    lit.assign(9, 0);
    //
    str.at(1)="getunits";
    menusystem.addaction("clearvec", num, str, lit, blank, "", blank, "", "");
    //
    str.at(1)="getunits";
    str.at(2)="yourturn";
    lit.at(2)=1;
    menusystem.addaction("addtovec", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    lit.assign(9, 0);
    //
    str.at(2)="title HP MP CT";
    menusystem.addaction("getunitlist", num, str, lit, blank, "getunits", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(2)=1;
    str.at(3)="!";
    menusystem.addaction("showeffect", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    menusystem.addaction("showunitlist", num, str, lit, blank, "", blank, "", "");    
    //
    str.at(1)="yourturn";
    str.at(2)="1:Move\n2:Attack\n3:Wait\n";
    num.at(3)=1;
    num.at(4)=3;
    menusystem.addaction("showmenu", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
  menusystem.actionlist(aftera);
    //
    str.at(1)="yourturn";
    lit.at(1)=1;
    str.at(2)="CT";
    num.at(3)=-20;
    num.at(4)=CURRENT;
    num.at(5)=RELATIVE;
    menusystem.addaction("modstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    lit.assign(9, 0);
    //
  menusystem.actionlist(controla);
    //
    num.at(1)=1;
    num.at(2)=3;
    menusystem.addaction("input", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    //
    
  menusystem.additem("Move", "", 0, 0, 1, NULL);
  menusystem.additem("Attack", "", 0, 0, 1, NULL);
  menusystem.additem("Wait", "Up\nLeft\nDown\nRight\n", 0, -1, 1, NULL);
  
  // TURN menu.
  turn      = menusystem.makemenu("TURN");
  menusystem.additem("Turn", "Finding which units turn is up...", 2, -1, 1, yourturn);
  menusystem.actionlist(controla);
    //
    str.at(1)="CT";
    num.at(2)=100;
    num.at(3)=ABSOLUTE;
    menusystem.addaction("findunit", num, str, lit, blank, "", blank, "", "yourturn");
    num.assign(9, 0);
    str.assign(9, "");
    //
  menusystem.actionlist(cancela);
    //
    num.at(1)=0;
    str.at(2)="CT";
    num.at(3)=8;
    num.at(4)=CURRENT;
    num.at(5)=RELATIVE;
    menusystem.addaction("modstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
  
  // SETUP menu.
  setup     = menusystem.addmenu(menusystem.makemenu("SETUP"));
  menusystem.additem("Setup", "Adding units to the battlefield...", 2, -1, 1, turn);
  menusystem.actionlist(beforea);
    //
    num.at(1)=1;
    str.at(2)="A";
    num.at(3)=1;
    num.at(4)=1;
    num.at(5)=3;
    num.at(6)=1;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=2;
    str.at(2)="B";
    num.at(3)=2;
    num.at(4)=1;
    num.at(5)=4;
    num.at(6)=1;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=3;
    str.at(2)="C";
    num.at(3)=3;
    num.at(4)=1;
    num.at(5)=5;
    num.at(6)=1;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=4;
    str.at(2)="D";
    num.at(3)=4;
    num.at(4)=6;
    num.at(5)=3;
    num.at(6)=2;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=5;
    str.at(2)="E";
    num.at(3)=5;
    num.at(4)=6;
    num.at(5)=4;
    num.at(6)=2;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=6;
    str.at(2)="F";
    num.at(3)=6;
    num.at(4)=6;
    num.at(5)=5;
    num.at(6)=2;
    menusystem.addaction("addunit", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="HP";
    num.at(3)=0;
    num.at(4)=10;
    num.at(5)=10;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="MP";
    num.at(3)=0;
    num.at(4)=5;
    num.at(5)=5;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="CT";
    num.at(3)=0;
    num.at(4)=100;
    num.at(5)=100;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="attack";
    num.at(3)=2;
    num.at(4)=2;
    num.at(5)=2;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="move";
    num.at(3)=3;
    num.at(4)=3;
    num.at(5)=3;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    num.at(1)=0;
    str.at(2)="range";
    num.at(3)=1;
    num.at(4)=1;
    num.at(5)=1;
    menusystem.addaction("addstat", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
    str.at(1)="yourturn";
    num.at(2)=1;
    menusystem.addaction("setnum", num, str, lit, blank, "", blank, "", "");
    num.assign(9, 0);
    str.assign(9, "");
    //
  
  return setup;
}


/*
menuitem *prepmenu()
{
  menuitem *menuer;
  menuaction *actioner;
  menuitem *setupmenu;
  menuitem *battlemenu;
  menuitem *turnmenu;


  // Battle Menu.
  battlemenu=new menuitem;
  menuer=battlemenu;

  menuer->title="yourturn";
  menuer->text="Move\nAttack\nWait\n";
  menuer->continuity=proceed;
  menuer->confirm=0;
  menuer->max=3;
  menuer->before=new menuaction;
  actioner=menuer->before;
    actioner->title="getxy";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="yourturn";
    actioner->literal.at(1)=-1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="getunitmap";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="gettargets";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(2)="yourx";
    actioner->literal.at(2)=-1;
    actioner->str.at(3)="youry";
    actioner->literal.at(3)=-1;
    actioner->num.at(4)=0;
    actioner->num.at(5)=0;
    actioner->num.at(6)=0;
    actioner->num.at(7)=0;
    actioner->num.at(8)=1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="clearvec";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="getunits";
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addtovec";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="getunits";
    actioner->str.at(2)="yourturn";
    actioner->literal.at(2)=-1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="getunitlist";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->title1="getunits";
    actioner->str.at(2)="title HP MP CT";
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="showeffect";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(2)=1;
    actioner->str.at(3)="!";
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="showunitlist";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="showmenu";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="Yourturn";
    actioner->str.at(2)="1:Move\n2:Attack\n3:Wait\n";
    actioner->num.at(3)=1;
    actioner->num.at(4)=3;
    actioner->next=NULL;
  menuer->after=NULL;
  menuer->idle=NULL;
  menuer->control=new menuaction;
  actioner=menuer->control;
    actioner->title="input";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=1;
    actioner->num.at(2)=3;
    actioner->next=NULL;
  menuer->cancel=NULL;
  menuer->next=new menuitem;
  menuer=menuer->next;

    menuer->title="Move";
    menuer->text="";
    menuer->continuity=restart;
    menuer->confirm=0;
    menuer->max=1;
    menuer->before=new menuaction;
    actioner=menuer->before;
      actioner->title="getstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="move";
      actioner->num.at(3)=CURRENT;
      actioner->record="move";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitmap";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="gettargets";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(2)="yourx";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="youry";
      actioner->literal.at(3)=-1;
      actioner->str.at(4)="move";
      actioner->literal.at(4)=-1;
      actioner->num.at(5)=2;
      actioner->num.at(6)=0;
      actioner->num.at(7)=1;
      actioner->num.at(8)=1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showeffect";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(2)=1;
      actioner->str.at(3)="[";
      actioner->next=NULL;
    menuer->after=new menuaction;
    actioner=menuer->after;
      actioner->title="move";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="movex";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="movey";
      actioner->literal.at(3)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="modstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="CT";
      actioner->num.at(3)=-40;
      actioner->num.at(4)=CURRENT;
      actioner->num.at(5)=RELATIVE;
      actioner->next=NULL;
    menuer->idle=NULL;
    menuer->control=new menuaction;
    actioner=menuer->control;
      actioner->title="getunitmap";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="setstr";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="error";
      actioner->str.at(2)="";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="Select destination.\nx:";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourx";
      actioner->literal.at(1)=-1;
      actioner->num.at(2)=0;
      actioner->record="movex";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="y:";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="youry";
      actioner->literal.at(1)=-1;
      actioner->num.at(2)=0;
      actioner->record="movey";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="setstr";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="error";
      actioner->str.at(2)="Select within move range.\n";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="inrange";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourx";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="youry";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="movex";
      actioner->literal.at(3)=-1;
      actioner->str.at(4)="movey";
      actioner->literal.at(4)=-1;
      actioner->str.at(5)="move";
      actioner->literal.at(5)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="setstr";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="error";
      actioner->str.at(2)="";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="gettargets";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(2)="movex";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="movey";
      actioner->literal.at(3)=-1;
      actioner->num.at(4)=0;
      actioner->num.at(5)=0;
      actioner->num.at(6)=0;
      actioner->num.at(7)=0;
      actioner->num.at(8)=1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showeffect";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(2)=1;
      actioner->str.at(3)="!";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="Move here?\n1:Yes 0:Cancel :";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(1)=1;
      actioner->num.at(2)=0;
      actioner->next=NULL;
    menuer->cancel=new menuaction;
    actioner=menuer->cancel;
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="error";
      actioner->literal.at(1)=-1;
      actioner->next=NULL;
    menuer->next=new menuitem;
    menuer=menuer->next;

    menuer->title="Attack";
    menuer->text="";
    menuer->continuity=restart;
    menuer->confirm=0;
    menuer->max=1;
    menuer->before=new menuaction;
    actioner=menuer->before;
      actioner->title="getstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="range";
      actioner->num.at(3)=CURRENT;
      actioner->record="range";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitmap";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="gettargets";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(2)="yourx";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="youry";
      actioner->literal.at(3)=-1;
      actioner->str.at(4)="range";
      actioner->literal.at(4)=-1;
      actioner->num.at(5)=0;
      actioner->num.at(6)=-1;
      actioner->num.at(7)=1;
      actioner->num.at(8)=1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showeffect";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(2)=1;
      actioner->str.at(3)="[";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->title1="getunits";
      actioner->str.at(2)="title HP MP CT";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->vec1.push_back(0);
      actioner->str.at(2)="title init";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="Select target init:\n:";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(1)=1;
      actioner->num.at(2)=0;
      actioner->record="target";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getxy";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="target";
      actioner->literal.at(1)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="gettargets";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(2)="yourx";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="youry";
      actioner->literal.at(3)=-1;
      actioner->num.at(4)=0;
      actioner->num.at(5)=0;
      actioner->num.at(6)=0;
      actioner->num.at(7)=0;
      actioner->num.at(8)=0;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showeffect";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(2)=1;
      actioner->str.at(3)="!";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="clearvec";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="getunits";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="addtovec";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="getunits";
      actioner->str.at(2)="yourturn";
      actioner->literal.at(2)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="addtovec";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="getunits";
      actioner->str.at(2)="target";
      actioner->literal.at(2)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->title1="getunits";
      actioner->str.at(2)="title HP MP CT";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showmessage";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="Attack this unit?\n1:Yes 0:Cancel :";
      actioner->next=NULL;
    menuer->after=new menuaction;
    actioner=menuer->after;
      actioner->title="modstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="target";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="HP";
      actioner->num.at(3)=-2;
      actioner->num.at(4)=CURRENT;
      actioner->num.at(5)=RELATIVE;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="modstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="CT";
      actioner->num.at(3)=-40;
      actioner->num.at(4)=CURRENT;
      actioner->num.at(5)=RELATIVE;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="clearvec";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="getunits";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="addtovec";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="getunits";
      actioner->str.at(2)="target";
      actioner->literal.at(2)=-1;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="getunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->title1="getunits";
      actioner->str.at(2)="title HP MP CT";
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="showunitlist";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->next=NULL;
    menuer->idle=NULL;
    menuer->control=new menuaction;
    actioner=menuer->control;
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(1)=1;
      actioner->num.at(2)=0;
      actioner->next=new menuaction;
      actioner=actioner->next;
      //
      actioner->title="inrange";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourx";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="youry";
      actioner->literal.at(2)=-1;
      actioner->str.at(3)="targetx";
      actioner->literal.at(3)=-1;
      actioner->str.at(4)="targety";
      actioner->literal.at(4)=-1;
      actioner->str.at(5)="range";
      actioner->literal.at(5)=-1;
      actioner->next=NULL;
    menuer->cancel=NULL;
    menuer->next=new menuitem;
    menuer=menuer->next;

    menuer->title="Wait";
    menuer->text="Up\nLeft\nDown\nRight\n";
    menuer->continuity=end;
    menuer->confirm=0;
    menuer->max=1;
    menuer->before=new menuaction;
    actioner=menuer->before;
      actioner->title="showmenu";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="Wait";
      actioner->str.at(2)="1:Up\n2:Left\n3:Down\n4:Right\n";
      actioner->num.at(3)=1;
      actioner->num.at(4)=4;
      actioner->next=NULL;
    menuer->after=new menuaction;
    actioner=menuer->after;
      actioner->title="modstat";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->str.at(1)="yourturn";
      actioner->literal.at(1)=-1;
      actioner->str.at(2)="CT";
      actioner->num.at(3)=-20;
      actioner->num.at(4)=CURRENT;
      actioner->num.at(5)=RELATIVE;
      actioner->next=NULL;
    menuer->idle=NULL;
    menuer->control=new menuaction;
    actioner=menuer->control;
      actioner->title="input";
      actioner->num.resize(9);
      actioner->str.resize(9);
      actioner->literal.resize(9, 0);
      actioner->num.at(1)=1;
      actioner->num.at(2)=4;
      actioner->next=NULL;
    menuer->cancel=NULL;
    menuer->next=NULL;



  // Turn Menu.
  turnmenu=new menuitem;
  menuer=turnmenu;

  menuer->title="Turn";
  menuer->text="Finding which units turn is up.\n";
  menuer->continuity=newmenu;
  menuer->newmenu=battlemenu;
  menuer->confirm=-1;
  menuer->max=1;
  menuer->before=NULL;
  menuer->after=NULL;
  menuer->idle=NULL;
  menuer->control=new menuaction;
  actioner=menuer->control;
    actioner->title="findunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="CT";
    actioner->num.at(2)=100;
    actioner->num.at(3)=ABSOLUTE;
    actioner->record="yourturn";
    actioner->next=NULL;
  menuer->cancel=new menuaction;
  actioner=menuer->cancel;
    actioner->title="modstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="CT";
    actioner->num.at(3)=8;
    actioner->num.at(4)=CURRENT;
    actioner->num.at(5)=RELATIVE;
    actioner->next=NULL;
  menuer->next=NULL;



  // Setup Menu.
  setupmenu=new menuitem;
  menuer=setupmenu;

  menuer->title="Setup";
  menuer->text="Adding units to battlefield.\n";
  menuer->continuity=newmenu;
  menuer->newmenu=turnmenu;
  menuer->confirm=-1;
  menuer->max=1;
  menuer->before=new menuaction;
  actioner=menuer->before;
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=1;
    actioner->str.at(2)="A";
    actioner->num.at(3)=1;
    actioner->num.at(4)=1;
    actioner->num.at(5)=3;
    actioner->num.at(6)=1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=2;
    actioner->str.at(2)="B";
    actioner->num.at(3)=2;
    actioner->num.at(4)=1;
    actioner->num.at(5)=4;
    actioner->num.at(6)=1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=3;
    actioner->str.at(2)="C";
    actioner->num.at(3)=3;
    actioner->num.at(4)=1;
    actioner->num.at(5)=5;
    actioner->num.at(6)=1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=4;
    actioner->str.at(2)="D";
    actioner->num.at(3)=4;
    actioner->num.at(4)=6;
    actioner->num.at(5)=3;
    actioner->num.at(6)=2;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=5;
    actioner->str.at(2)="E";
    actioner->num.at(3)=5;
    actioner->num.at(4)=6;
    actioner->num.at(5)=4;
    actioner->num.at(6)=2;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addunit";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=6;
    actioner->str.at(2)="F";
    actioner->num.at(3)=6;
    actioner->num.at(4)=6;
    actioner->num.at(5)=5;
    actioner->num.at(6)=2;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="HP";
    actioner->num.at(3)=0;
    actioner->num.at(4)=10;
    actioner->num.at(5)=10;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="MP";
    actioner->num.at(3)=0;
    actioner->num.at(4)=5;
    actioner->num.at(5)=5;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="CT";
    actioner->num.at(3)=0;
    actioner->num.at(4)=100;
    actioner->num.at(5)=100;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="attack";
    actioner->num.at(3)=2;
    actioner->num.at(4)=2;
    actioner->num.at(5)=2;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="move";
    actioner->num.at(3)=3;
    actioner->num.at(4)=3;
    actioner->num.at(5)=3;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="addstat";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->num.at(1)=0;
    actioner->str.at(2)="range";
    actioner->num.at(3)=1;
    actioner->num.at(4)=1;
    actioner->num.at(5)=1;
    actioner->next=new menuaction;
    actioner=actioner->next;
    //
    actioner->title="setnum";
    actioner->num.resize(9);
    actioner->str.resize(9);
    actioner->literal.resize(9, 0);
    actioner->str.at(1)="yourturn";
    actioner->num.at(2)=1;
    actioner->next=NULL;
  menuer->after=NULL;
  menuer->idle=NULL;
  menuer->control=NULL;
  menuer->cancel=NULL;
  menuer->next=NULL;
  
  return setupmenu;
}
*/
