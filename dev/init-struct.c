struct st
{
  string title;
  int value[3];

  st *next;
};


struct item
{
  char title[8];
  char filename[8];

  int quantity[2];
  st *stats;

  item *next;
};


struct list
{
  char title[8];
  item *start;

  list *next;
};


struct unit
{
  string title;
  char avatar;

  int x, y;

  int team;
  int out;

  st *stats;
  list *lists;
  
  int init;
  unit *next;
};


unit *start=new unit;
