#include <string>
#include <vector>

using namespace std;

struct menuaction
{
  string title;

  vector<int> num;
  vector<string> str;
  vector<int> literal;

  vector<int> vec1; string title1;
  vector<int> vec2; string title2;
  
  string record;  // Variable to record result.

  menuaction *next;
};

struct menuitem
{
  string title;
  string text;

  int empty;

  int continuity;
  int cursor;
  int selection;
  string def;
  int memory;
  int min, max;
  int confirm;
  int cancelable;

  menuaction *control;
  menuaction *before, *after, *idle;
  menuaction *cancel;

  menuitem *newmenu;

  menuitem *next;
};

struct menulister{
  string title;
  menuitem *menu;

  menulister *next;
};

struct variable
{
  string title;

  int num;
  string str;
  vector<int> vec;

  variable *next;
};
