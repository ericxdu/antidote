#include <ncurses.h>


WINDOW *wmenu;     // Declaring pointers for each different window.
WINDOW *wmesg;
WINDOW *wmap;

class display_ncurses {
 public:
  int matrix();
  int menu(char title[], char menu[], int cursor, int max);
  int message(char mesg[]);
  int confirm();
  int cancel();
  int units();
  int drawall();

  display_ncurses();
  ~display_ncurses();
};


display_ncurses::display_ncurses()
{
  initscr();

  wmenu = newwin(15, 15, 50, 20);     // Declaring windows for each window pointer.
  box(wmenu, 0, 0);
  wmesg = newwin(5, 30, 70, 20);
  box(wmesg, 0, 0);
  wmap = newwin(45, 45, 5, 0);
  box(wmap, 0, 0);
}


display_ncurses::~display_ncurses()
{
  endwin();     // Stop ncurses mode.
}


int display_ncurses::matrix()
{
  box(wmap, 0, 0);

  mvwprintw(wmap, 1, 1, "display_matrix");

  wrefresh(wmap);
}


int display_ncurses::menu(char title[], char menu[], int cursor, int max)
{
  box(wmenu, 0, 0);

  int letter=1, line=1, l;

  mvwprintw(wmenu, 0, 0, "[");
  waddstr(wmenu, title);
  wprintw(wmenu, "]");

  mvwaddstr(wmenu, 1, 2, menu);

  wrefresh(wmenu);
}


int display_ncurses::message(char mesg[])
{
  box(wmesg, 0, 0);

  mvwaddstr(wmesg, 1, 1, mesg);

  wrefresh(wmesg);
}


int display_ncurses::confirm()
{
}


int display_ncurses::cancel()
{
}


int display_ncurses::units()
{
}


int display_ncurses::drawall()
{
  refresh();
  wrefresh(wmap); wclear(wmap);
  wrefresh(wmenu); wclear(wmenu);
  wrefresh(wmesg); wclear(wmesg);
}
