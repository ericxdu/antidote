int menu::setnum(string title, int num)
{
  variable *varptr;


  varptr=variables;
  if(varptr!=NULL){
    while(varptr->title != title)
      if(varptr->next == NULL){
        varptr->next=new variable;
        varptr=varptr->next;

        if(varptr!=NULL){
          varptr->title=title;
          varptr->next=NULL;
        }else
          return -1;
      }else
        varptr=varptr->next;


  }else{
    varptr=new variable;
    variables=varptr;

    if(varptr!=NULL){
      varptr->title=title;
      varptr->next=NULL;
    }else
      return -1;
  }


  if(varptr->title==title)
    varptr->num=num;


  return 0;
}


int menu::setstr(string title, string str)
{
  variable *varptr;


  varptr=variables;
  if(varptr!=NULL){
    while(varptr->title != title)
      if(varptr->next == NULL){
        varptr->next=new variable;
        varptr=varptr->next;

        if(varptr!=NULL){
          varptr->title=title;
          varptr->next=NULL;
        }else
          return -1;
      }else
        varptr=varptr->next;


  }else{
    varptr=new variable;
    variables=varptr;

    if(varptr!=NULL){
      varptr->title=title;
      varptr->next=NULL;
    }else
      return -1;
  }


  if(varptr->title==title)
    varptr->str=str;


  return 0;
}


int menu::setvec(string title, vector<int> vec)
{
  variable *varptr;


  varptr=variables;
  if(varptr!=NULL){
    while(varptr->title != title)
      if(varptr->next == NULL){
        varptr->next=new variable;
        varptr=varptr->next;

        if(varptr!=NULL){
          varptr->title=title;
          varptr->next=NULL;
        }else
          return -1;
      }else
        varptr=varptr->next;


  }else{
    varptr=new variable;
    variables=varptr;

    if(varptr!=NULL){
      varptr->title=title;
      varptr->next=NULL;
    }else
      return -1;
  }


  if(varptr->title==title)
    varptr->vec=vec;

  return 0;
}


int menu::addtovec(string title, int num)
{
  variable *varptr;


  varptr=variables;
  if(varptr!=NULL){
    while(varptr->title != title)
      if(varptr->next == NULL){
        varptr->next=new variable;
        varptr=varptr->next;

        if(varptr!=NULL){
          varptr->title=title;
          varptr->next=NULL;
        }else
          return -1;
      }else
        varptr=varptr->next;


  }else{
    varptr=new variable;
    variables=varptr;

    if(varptr!=NULL){
      varptr->title=title;
      varptr->next=NULL;
    }else
      return -1;
  }


  if(varptr->title==title)
    varptr->vec.push_back(num);

  return 0;
}


int menu::clearvec(string title)
{
  variable *varptr;


  varptr=variables;
  if(varptr!=NULL){
    while(varptr->title != title)
      if(varptr->next!=NULL)
        varptr=varptr->next;
      else
        return -1;

  }else
    return -1;


  if(varptr->title==title)
    varptr->vec.clear();


  return 0;
}


int menu::getnum(string title)
{
  variable *varptr;


  varptr=variables;
  while(varptr!=NULL){
    if(varptr->title==title){
      return varptr->num;
    }

    varptr=varptr->next;
  }

  return 0;
}


string menu::getstr(string title)
{
  variable *varptr;


  varptr=variables;
  while(varptr!=NULL){
    if(varptr->title==title)
      return varptr->str;

    varptr=varptr->next;
  }

  return "";
}


vector<int> menu::getvec(string title)
{
  variable *varptr;


  varptr=variables;
  while(varptr!=NULL){
    if(varptr->title==title)
      return varptr->vec;

    varptr=varptr->next;
  }
}
