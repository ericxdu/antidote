struct param
{
  int one, two;
  param *next;
};

struct graph
{
  char image;
  param *parameters;
  graph *next;
};

struct frame
{
  graph *graphics;
  frame *next;
};

struct effect
{
  frame *step;
  frame *frames;
  effect *next;
};

struct fieldimage
{
  int step;
  graph *statics;
  frame *frames;
  fieldimage *next;
};

struct avatar
{
  char image;
  avatar *next;
};
