#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#include "init-struct.c"

class init
{
 public:
  int addunit(int index, string title, int avatar, int x, int y, int team);
  int replunit(int index, int replace);
  int delunit(int index);

  int addstat(int index, string title, int min, int cur, int max);
  int delstat();
  int findunit(string title, int key, int absolute);
  int getstat(int index, string title, int element);
  int modstat(int index, string title, int factor, int element, int absolute);

  int findvictor();
  int out(int index, int toggle);
  int output();
  int getunits(vector<int> units, string stats, string &ulist);
  int unitexist(int index);
  int getavatar(int index);
  int move(int index, int x, int y);
  int getnick(int index, char *title);
  int getteam(int index);
  int getxy(int index, int *x, int *y);

};

#include "init-unit.c"
#include "init-stat.c"
#include "init-list.c"
#include "init-misc.c"
