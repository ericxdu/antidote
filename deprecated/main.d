#include "display.c"
#include "control.c"
#include "init.c"
#include <iostream>

using namespace std;

int debug()
{
  int x, y, n;
  int units, teams;

  cout << "Enter number of units in battle: ";
  cin >> units;
  cout << "Enter number of teams in battle: ";
  cin >> teams;

  for(n=1; n<=units; n++){
    y = n/8+1;
    x = y*8-n;
    init_addunit(0, n+64, x, y, n/(units/teams));
    init_addstat(n, "balance", 0, 10, 10);
  }
}

int clock_tick()
{
  int yourturn, yourmenu=0;  // Init of current unit, first menu selection.
  int x=0, y=0, yourx=0, youry=0;  // Selected x y, current x y.
  int target=0, power=0, re;   // Target initnum, attack power, initlist browser.
  int teams, units, n;     // Number of teams, number of units, n variable.
  int victor, team;        // Init of victor, team number of current unit.
  int confirm=0;             // Confirmation variable.
  char nick;               // Name of current unit.

  while(init_victory() < 1){
    if(init_victory() == 0){

      yourturn = init_findunit("balance", 10, false);
      if(yourturn == 0)
	init_modstat(0, "balance", 1, +1, false);
      else{
	init_output();
	init_getxy(yourturn, &yourx, &youry);
	cout << "yourturn: " << yourturn << endl << endl;
	cout << "yourmenu: " << yourmenu << endl;
	cout << "x: " << x << " y: " << y << endl;
	cout << "yourx: " << yourx << " youry: " << youry << endl;
	cout << "target: " << target << " power: " << power << endl << endl;
	cout << "confirm: " << confirm << endl << endl;


        // Menu system for controlling the current unit.


	if(confirm != 0){
	  cout << "unconfirm section\n";
	  if(yourmenu > 0){
	    if(x > 0){
	      cout << "move menu\n";
	      display_matrix();
	      display_message("Enter destination coordinates: ");
	      control_input(&x, 0);
	      control_input(&y, 0);
	      display_matrix();
	    }else if(target > 0){
	      if(power > 0){
		cout << "power menu\n";
		display_menu("attack power", "1\n2\n3\n", 1, 3);
		display_message("Enter attack power: ");
		control_input(&power, 3);
	      }else{
		cout << "target menu\n";
		display_matrix();
		display_menu("target", "\n", 1, 1);
		display_message("Enter target initiative number: ");
		control_input(&target, 0);
	      }
	    }else{
	      cout << "yourturn menu\n";
	      display_menu("yourturn", "1: move\n2: attack\n", 1, 3);
	      display_message("Enter choice: ");
	      control_input(&yourmenu, 2);
	    }
	  }
	  cout << "section end\n";
	}
	display_confirm();
	confirm = control_confirm();


        /* What to do when the user enters "confirm" input. */


	cout << "yourturn: " << yourturn << endl << endl;
	cout << "yourmenu: " << yourmenu << endl;
	cout << "x: " << x << " y: " << y << endl;
	cout << "yourx: " << yourx << " youry: " << youry << endl;
	cout << "target: " << target << " power: " << power << endl << endl;
	cout << "confirm: " << confirm << endl << endl;
	if(confirm == 0){
	  cout << "confirm section\n";
	  if(yourmenu > 0){
	    if(x > 0){
	      init_move(yourturn, x, y);
	      cout << "move\n";
	      yourmenu = 0;
	      x = 0;
	      y = 0;
	      target = 0;
	      power = 0;
	    }else if(target > 0){
	      if(power > 0){
		cout << "attack\n";
		re = 1;
		while(init_modstat(re, "balance", false, 1, 0)){
		  if(re != yourturn && re != target)
		    init_modstat(re, "balance", false, 1, +1);
		  else
		    init_modstat(re, "balance", false, 1, -1);
		}
		yourmenu = 0;
		x = 0;
		y = 0;
		target = 0;
		power = 0;
	      }else
		power = 1;
	    }else if(yourmenu == 1){
	      cout << "goto movemenu\n";
	      x = yourx;
	      y = youry;
	    }else if(yourmenu == 2){
	      cout << "goto attackmenu\n";
	      target = 1;
	    }else
	      cout << "!! Error: invalid selection\n";
	  }else{
	    cout << "goto yourmenu\n";
	    yourmenu = 1;
	  }
	  confirm = -1;
	}
	cout << "section end\n\n\n";
      }
    }else{
      if(confirm != 0){
	if(units > 0){
	  if(teams > 0){
	    display_menu("teams", " ", 1, 1);
	    display_message("Enter the number of teams: ");
	    control_input(&teams, 0);
	  }else{
	    display_menu("units", " ", 1, 1);
	    display_message("Enter the number of units: ");
	    control_input(&units, 0);
	  }
	}else
	  teams = 0;
      }
      if(confirm == 0){
	if(units > 0){
	  if(teams > 0)
	    for(n = 1; n < units; n ++){
	      y = n/8+1;
	      x = y*8-n;
	      init_addunit(0, n+64, x, y, n/(units/teams));
	      init_addstat(n, "balance", 0, 10, 10);
	    }
	  else
	    teams = 1;
	}else{
	  units = 2;
	  teams = 0;
	}
	confirm = false;
      }
    }
  }
}


int main()
{

  debug();  // Runs the debug function for testing without file input.
  while(true){
    clock_tick();
  }
}
