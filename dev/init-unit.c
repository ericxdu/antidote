



// Function to add unit to any slot in the initiative list.
int init::addunit(int index, string title, int avatar, int x, int y, int team)
{
  unit *browser;
  unit *finger=new unit;  // *finger and *sever are initialized to simplify following IF tree.
  unit *sever=new unit;

  int init=1;

  browser=start;
  while(browser!=NULL){

    if(init==index){

      if(browser->x==x && browser->y==y){                                  // Error if space is occupied by another unit,
	cout << "!! Error: position occupied " << x << "," << y << ".\n";  // this will likely be optional in the future.
	return false;

      }else{                      // This tree figures out where to move pointers and how to add the new unit.
	if(sever->init==0){
	  if(start->init==0){
	    finger->next=start->next;  // Add unit to empty list.
	    start=finger;
	  }else{
	    sever=start;               // Insert unit in first slot.
	    finger->next=sever->next;
	    sever->next=finger;
	  }
	}else{
	  sever=sever->next;           // Insert unit in specified slot.
	  finger->next=sever->next;
	  sever->next=finger;
	}
  finger->title=title;
  finger->avatar=avatar;
	finger->x=x;
	finger->y=y;
	finger->team=team;
	finger->init=-1;
	finger->out=-1;
	return true;
      }
    }

    if(browser->next!=NULL){
      if(sever->init==0)
	delete sever;
      sever=browser;           // *sever points to unit directly before browser.
      browser=browser->next;
      init++;                  // Advance *browser and init.

    }else if(init+1 == index)  // If index is exactly at the end of the list, init will equal index.
      init++;

    else if(index==0)          // If index is 0, force init to equal index.
      init=index;

    else{
      cout << "!!Error: Space for new unit not found.\n";     // Error if index is larger than end of list.
      return false;
    }

  }
}


// Function to move unit from one slot to another slot in initiative list.
int init::replunit(int index, int replace)
{
}


// Function to remove unit from initiative list.
int init::delunit(int index)
{
}
