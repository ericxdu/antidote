#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "menu.h"

using namespace std;

int loadmenu(string filename);

menu menusystem;

int main()
{
  int cstrsize = 1024;
  char menucstr[cstrsize];
  char delim;

  string menustring;


  ifstream menufile("menu.mnu");

  menufile.get(menucstr, cstrsize, delim);
  menustring=menucstr;

  cout << menustring;

  loadmenu("menu.mnu");
  menusystem.loadmenu("menu.mnu");
}


int loadmenu(string filename)
{
  // menuitem variables.
  string title;
  string text;
  int confirm;
  int continuity;
  int max;

  // menuaction variables.
  string action;
  vector<int> num;    num.assign(9, 0);
  vector<string> str; str.assign(9, "");
  vector<int> lit;    lit.assign(9, 0);
  vector<int> vec1, vec2;
  string record;

  int cstrsize = 1024;
  char menucstr[cstrsize];
  char delim;
  char blank = 32;
  char endline = (char)"\n";

  string menu;
  int m = 0;
  int m2 = 0;
  string output;

  int t=0, t2;
  int b, b2;
  int r, r2;
  int a=string::npos, a2=string::npos;

  ifstream menufile("menu.mnu");
  menufile.get(menucstr, cstrsize, delim);
  menu=menucstr;

  string readint;
  int readerint;
  string readerstring;
  vector<int> readervector;
  int par, vec;
  int n;

  
  while(t < menu.length()){
    t = menu.find_first_not_of(" \n", t);
    t2 = menu.find_first_of(" \n", t);

    if(t != string::npos){
      if(menu.compare(t, t2-t, "TITLE") == 0  ||  menu.compare(t, t2-t, "TEXT") == 0){
                                                    // Find string and enclose in b-b2 brackets.
        b = menu.find("\"", t);
        b2 = menu.find("\"", b+1);
        r = b;
                
      }else if(menu.compare(t, t2-t, "MAX") == 0){  // Find integer and enclose in b-b2 brackets.
        b = menu.find_first_of(" \n", t);
        b = menu.find_first_not_of(" \n", b);
        b2 = menu.find_first_of(" \n", b);
        r = b;
        
      }else if(menu.compare(t, t2-t, "BEFORE") == 0  ||  menu.compare(t, t2-t, "CONTROL") == 0  ||
               menu.compare(t, t2-t, "AFTER")  == 0  ||  menu.compare(t, t2-t, "CANCEL")  == 0  ||
               menu.compare(t, t2-t, "IDLE")   == 0){
        if(a == string::npos){
          if(a2 == string::npos)
            a2 = menu.find_first_not_of(" \n", t2) - 1;

          if(a2 != string::npos  &&  a2 != menu.length())
            a = menu.find_first_not_of(" \n", a2+1);

          if(a != string::npos){
            if(menu.substr(a, 1) == "["){
              a2 = menu.find("]", a);
              if(a2 == string::npos)
                a2 = menu.length();
              b2=a;
              vec=1;
              par=1;
              cout << menu.substr(t, t2-t) << menu.substr(a, a2-a+1) << endl;
            }else if(menu.substr(a, 1) == "'"){
              a2 = menu.find("'", a+1);
              if(a2 == string::npos)
                a2 = menu.length();
              b2=a;
              cout << menu.substr(t, t2-t) << menu.substr(a, a2-a+1) << endl;
            }else{
              t=a;
              a = string::npos;
              a2 = string::npos;
              r = string::npos;
            }
          }else{
            t = string::npos;
            a = string::npos;
            a2 = string::npos;
            r = string::npos;
          }
        }

        if(a != string::npos){
          b = menu.find_first_not_of(" \n", b2+1);
          if(b != string::npos){
            if(menu.substr(b, 1) == "\""  ||  menu.substr(b, 1) == "'"){
              b2 = menu.find(menu.at(b), b+1);
            }else if(menu.substr(b, 1) == "("){
              b2 = menu.find(")", b);
            }else{
              b2 = menu.find_first_of(" \n", b) - 1;
            }

            if(b2 >= a2)
              b2 = a2-1;
            r=b;
            if(b2 < b){
              // Output last action.
              cout << "  action:" << action << endl;
              cout << "  num:";
              for(n=1; n<9; n++)
                cout << num.at(n) << " ";
              cout << endl;
              cout << "  str:";
              for(n=1; n<9; n++)
                cout << "\"" << str.at(n) << "\" ";
              cout << endl;
              cout << "  lit:";
              for(n=1; n<9; n++)
                cout << lit.at(n) << " ";
              cout << endl;
              cout << "  vec1:";
              for(n=1; n<vec1.size(); n++)
                cout << vec1.at(n) << " ";
              cout << endl;
              cout << "  vec2:";
              for(n=1; n<vec2.size(); n++)
                cout << vec2.at(n) << " ";
              cout << endl;
              cout << "  record:" << record << endl;
              
              a = string::npos;
              r = string::npos;
            }else
              cout << menu.substr(b, 0);
          }
        }

      }else{
        r = string::npos;

      }
    }


    if(r != string::npos)
      if(menu.substr(r, 1) == "\""  ||  menu.substr(r, 1) == "'"  ||  menu.substr(r, 1) == "*"){
        if(menu.substr(r, 1) == "*")
          readerstring.assign(menu, b+1, b2-b);
        else
          readerstring.assign(menu, b+1, b2-b-1);
      }else if(menu.substr(r, 1) == "("){

      }else{
        readerstring.assign(menu, b, b2-b+1);
        readerint = atoi(readerstring.c_str());
      }


    if(t != string::npos){
      if(menu.compare(t, t2-t, "TITLE") == 0){
        title = readerstring;
        t = t2;
      }else if(menu.compare(t, t2-t, "TEXT") == 0){
        text = readerstring;
        t = t2;
      }else if(menu.compare(t, t2-t, "MAX") == 0){
        max = readerint;
        t = t2;
      }else{
        if(menu.compare(t, t2-t, "BEFORE")  == 0  ||
           menu.compare(t, t2-t, "CONTROL") == 0  ||
           menu.compare(t, t2-t, "AFTER")   == 0  ||
           menu.compare(t, t2-t, "CANCEL")  == 0  ||
           menu.compare(t, t2-t, "IDLE")    == 0){

          if(a != string::npos  &&  menu.substr(a, 1) == "["){
            if(menu.substr(b, 1) == "*"){
              action = readerstring;
            }else if(menu.substr(b, 1) == "\""){
              str.at(par) = readerstring;
              par++;  // Move to next parameter.
            }else if(menu.substr(b, 1) == "'"){
              str.at(par) = readerstring;
              lit.at(par) = -1;
              par++;  // Move to next parameter.
            }else if(menu.substr(b, 1) == "("){
              if(vec==1)            // Action vectors.
                vec1 = readervector;
              else if(vec==2)
                vec2 = readervector;
              vec++;  // Move to next vector.
            }else{
              num.at(par) = readerint;
              par++;
            }
          }else if(a != string::npos  &&  menu.substr(a, 1) == "'"){
            record = readerstring;  // Name of record variable.
          }            
        }else
          t=t2;
      }
    }
  }

  return 0;
}
