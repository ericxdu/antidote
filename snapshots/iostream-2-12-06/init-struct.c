struct stat
{
  char title[8];
  int value[3];

  stat *next;
};


struct item
{
  char name[8];
  char filename[8];

  int quantity[2];
  stat *stats;

  item *next;
};


struct list
{
  char header[8];
  item *start;

  list *next;
};


struct unit
{
  char nick[8];

  stat *stats;
  list *lists;

  int x, y;

  int team;
  int out;

  int init;
  unit *next;
};


unit *start=new unit;
