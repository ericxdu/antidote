class display_iostream {
 public:
  int matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team);
  int menu(char title[], char menu[], int cursor, int max);
  int message(char mesg[]);
  int confirm();
  int cancel();
  int units();
  int drawall();

  display_iostream();
  ~display_iostream();
};


display_iostream::display_iostream()
{
}


display_iostream::~display_iostream()
{
}


int display_iostream::matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team)
{
  cout << "display_matrix\n";

  int sx, sy;
  int dist, distx, disty;
  int ally;
  char nick[8];

  int matrix[size][size];
  int init=1;

  for(sy=0; sy<size; sy++){
    for(sx=0; sx<size; sx++){
      matrix[sx][sy] = 0;              // Change all elements to 0;
    }
  }

  while(init_getxy(init, &sx, &sy)){
    if(sx>=ox && sy>=oy)
      if(sx < ox+size  &&  sy < oy+size)
	matrix[sx-ox][sy-oy] = init;     // Put units on the matrix.
    init++;
  }

  cout << "  ";
  for(sx=0; sx<size; sx++){
    cout << " " << sx+ox;              // Print x coordinates across the top of the grid.
    if(sx+ox < 9)
      cout << " ";
  }
  cout << endl;

  for(sy=0; sy<size; sy++){
    cout << sy+oy;
    if(sy < 10)
      cout << " ";
    for(sx=0; sx<size; sx++){

      init=matrix[sx][sy];

      dist = distance(x-ox, y-oy, sx, sy);

      if(init==0)
	if(unocc==0 && dist<=range)
	  cout << "[ ]";
	else
	  cout << "   ";
      else if(init_getteam(init) == team)
	if(occ==1 && dist<=range)
	  cout << "[" << init << "]";
	else
	  cout << " " << init << " ";
      else if(init_getteam(init) != team)
	if(occ==-1 && dist<=range)
	  cout << "[" << init << "]";
	else
	  cout << " " << init << " ";
      else
	cout << " " << init << " ";
    }
    cout << endl;
  }
}


int display_iostream::menu(char title[], char menu[], int cursor, int max)
{
  int letter = 1, line = 1;

  cout << "[" << title << "]" << endl;
  cout << menu;
  cout << "0: cancel\n";

  return true;
}


int display_iostream::message(char mesg[])
{
  cout << mesg;
}


int display_iostream::confirm()
{
  //cout << "[1:Confirm 0:Cancel]: ";
}


int display_iostream::drawall()
{
}
