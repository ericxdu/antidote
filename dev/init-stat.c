// Function to add a statistic to one or all units.
int init::addstat(int index, string title, int min, int cur, int max)
{
  unit *browser;
  st *finder;

  int init=0;
  if(index)     // if index is 0, init will stay at 0 and stat will be added for all units.
    init++;

  browser=start;
  while(browser!=NULL){
    if(init==index){
      
      if(browser->stats!=NULL){
	finder=browser->stats;
	while(finder->next!=NULL){
	  finder=finder->next;
	}
  finder->next=new st;
	finder=finder->next;
	finder->next=NULL;
      }else{
  finder=new st;
	finder->next=NULL;
	browser->stats=finder;
      }

      while(finder!=NULL){
	if(finder->next==0){
    finder->title=title;
	  finder->value[0]=min;
	  finder->value[1]=cur;
	  finder->value[2]=max;
	  if(finder->value[1] < finder->value[0])
	    finder->value[1] = finder->value[0];
	  if(finder->value[1] > finder->value[2])
	    finder->value[1] = finder->value[2];
	  break;
	}else{
	  finder=finder->next;
	}
      }

    }
    browser=browser->next;
    if(index)
      init++;
  }
}


// Function to remove a statistic from one or all units.
int init::delstat()
{
}


// Function to return the initiative number of one unit with a certain value in one statistic.
int init::findunit(string title, int key, int absolute)
{
  unit *browser;
  unit *finger=new unit;
  st *finder;
  st *sever=new st;

  sever->value[1]=0;
  int init=0;
  int winit=0;
  init++;

  browser=start;
  while(browser!=NULL){
    
    if(browser->out){
      if(browser->stats!=NULL){
        finder=browser->stats;
        while(finder!=NULL){
          if(finder->title==title){
            if(finder->value[1] > sever->value[1]){
              sever=finder;
              winit=init;
            }
          }
          finder=finder->next;
        }
      }
    }
    if(browser->next==NULL){
      return winit;
    }

    browser=browser->next;
    init++;
  }
}


// Function to return the value of one statistic of one unit.
int init::getstat(int index, string title, int element)
{
  unit *browser;
  st *finder;

  int init=0;
  init++;

  browser=start;
  while(browser!=NULL){
    if(init==index){
      if(browser->stats!=NULL){
	finder=browser->stats;
	while(finder!=NULL){
    //if(strcmp(finder->title, title) == 0)
    if(finder->title == title)
	    return finder->value[element];
	  finder=finder->next;
	}
      }
    }

    browser=browser->next;
    init++;
  }

  return 0;
}


// Function to modify the value of one statistic for one or all units.
int init::modstat(int index, string title, int factor, int element, int absolute)
{
  unit *browser;
  st *finder;
  st *sever;

  int init=0;
  if(index)
    init++;

  browser=start;
  while(browser!=NULL){
    if(init==index){

      if(browser->stats!=NULL){
      finder=browser->stats;
      while(finder!=NULL){
        if(finder->title == title){
          if(absolute==0)
            finder->value[element]=0;

          finder->value[element]+=factor;

          if(finder->value[1] < finder->value[0])
            finder->value[1] = finder->value[0];
          if(finder->value[1] > finder->value[2])
            finder->value[1] = finder->value[2];
        }
        finder=finder->next;
      }
    }
  }

  if(browser->next!=NULL){
    browser=browser->next;
    if(index)
      init++;
  }else if(index > init){
    cout << "!!Error: Unit not found.\n";
    return false;
  }else
    return true;
  }
}
