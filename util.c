int util::distance(int x, int y, int x2, int y2)
{
  int distx;
  int disty;

  distx = x-x2;
  disty = y-y2;
  if(distx > 0)
    if(disty > 0)
      return distx+disty;
    else
      return distx-disty;            // Calculate the square distance to each coordinate.
  else if(disty > 0)
    return disty-distx;
  else
    return -disty-distx;
}


int util::inrange(int x, int y, int x2, int y2, int range)
{
  int distx;
  int disty;
  int distance;

  distx = x-x2;
  disty = y-y2;
  if(distx > 0)
    if(disty > 0)
      distance=distx+disty;
    else
      distance=distx-disty;            // Calculate the square distance to each coordinate.
  else if(disty > 0)
    distance=disty-distx;
  else
    distance=-disty-distx;

  if(distance<=range)
    return 0;
  else
    return -1;
}


int util::getmap(vector<int> &alist, init &initlist)
{
  int x, y;
  int a=1;
  int initnum=1;

  alist.clear();

  while( initlist.unitexist(initnum) ){
    initlist.getxy(initnum, &x, &y);
    x=x*3;
    //battlefield.getxy(&x, &y);

    alist.push_back(initlist.getavatar(initnum));
    alist.push_back(x);
    alist.push_back(y);

    initnum++;
  }

  return true;
}


int util::gettargets(vector<int> &coors, int x, int y, int range, int occ, int unocc, int center, int team, init &initlist)
{
  int sx, sy, cx, cy, dx, dy;
  int c=0, n, cend, pair, set, sets[range];
  int alliance, initnum=1;
  int dist;
  vector<int> inits;

  if(center>-1)
    sy=center;
  else
    sy=0;

  coors.clear();

  c=0;
  if(unocc==0)
    for(sx=0; sx<=range; sx++){
      for(;  distance(x, y, x+sx, y+sy) <= range;  sy++)
        for(dy=-1; dy<=1; dy+=2)
          for(dx=-1; dx<=1; dx+=2)
            if(x+(sx*dx) > 0  &&  y+(sy*dy) > 0){
                cx = x+(sx*dx);
                cy = y+(sy*dy);
                //battlefield.getspacexy(&cx, &cy);
                cx=cx*3-1;
                coors.push_back(cx);
                coors.push_back(cy);
              } // If cx,cy are not above zero, coors[] is left unchanged but c moves on.
    sets[sx] = sy-1;

    if(center>-1)
      sy=center-1;
    else
      sy=0;

    }

  c=0;
  if(occ==-1 || occ==1 || unocc==0 && occ!=0 || unocc!=0 && occ==0)
    while(initlist.unitexist(initnum)){
    initlist.getxy(initnum, &cx, &cy);
    dist = distance(x, y, cx, cy);
    if(dist <= range  &&  dist >= center){
        if(team != initlist.getteam(initnum))
          alliance = -1;
        else if(team == initlist.getteam(initnum))
          alliance = 1;
        if(unocc==0){
          dx = cx-x;
          dy = cy-y;
          set=0;
          for(n=0;  n <= abs(dx);  n++)
            set += sets[n];
          pair=0;
          if(dx>0)
            pair+=4;
          if(dy>0)
            pair+=2;

          c = set*8+pair;
          if(center>-1)
            c-=(8*center);
          if(c < coors.size())
            if(alliance!=occ && occ!=0){
              coors.at(c)=0;
              coors.at(c+1)=0;
            }
        }else{
          if(alliance==occ || occ==0){
            //battlefield.getspacexy(&cx, &cy);
            cx=cx*3-1;
            //if(coors[c] > -1  &&  coors[c+1] > -1){
              coors.push_back(cx);
              coors.push_back(cy);
              inits.push_back(initnum);
              //c+=2;
            //}else{
            //  return -1;
            //}
          }
        } // EndIf unocc.
      } // EndIf distance.
      initnum++;
    } // LoopWhile unitexist.

  return 0;
}
