#include <vector>

using namespace std;

int MINIMUM = 0;     // For entering in "element" slot on functions.
int CURRENT = 1;
int MAXIMUM = 2;
    
int ABSOLUTE = 0;     // For entering in "absolute" slot on functions.
int RELATIVE = -1;
    
int ALLIES = 1;     // For entering in "occupied" slot in map/matrix function.
int EMPTY = 0;
int ENEMIES = -1;
int ANY = 2;
    
int UNOCCUPIED = 0;     // For entering in "unoccupied" slot in map/matrix function.
int OCCUPIED = -1;


class util
{
  public:
    int distance(int x, int y, int x2, int y2);
    int inrange(int x, int y, int x2, int y2, int range);
    int getmap(vector<int> &alist, init &initlist);
    int gettargets(vector<int> &coors, int x, int y, int range, int occ, int unocc, int center, int team, init &initlist);
};


#include "util.c"
