#include "init.h"
#include "util.h"
#include "io.h"
#include "menu.h"

#include "games\tactics\menus\menu.c"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

init initlist;


int main(int argc, char *argv[])
{
  util tools;
  display_iostream display;
  control_iostream control;
  
  int n;
  
  int fieldcoors[33] = {9, 5, 12, 5, 
                        10, 6, 11, 6, 
                        9, 7, 12, 7, -1};

  display.addfield("@", fieldcoors);
  for(n=1; n<=6; n++){
    display.addavatar((char)n+64);
  }

  menu menusystem(display, control, initlist, tools);   // Creates menu system.
                                                // !! Use only loadmenu or addmenu.
  //menusystem.addmenu(loadmenu(menusystem));   // Prepares a menu using menumaker.
  //menusystem.addmenu(prepmenu());   // Prepares a debug menu.
  menusystem.loadmenu("menu.mnu");
  menusystem.testmenu(menusystem.getmenu());

  menusystem.run();
}
